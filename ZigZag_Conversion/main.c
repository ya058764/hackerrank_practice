#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char * convert(char * s, int numRows){
    int len1 = strlen(s);
    if (numRows == 1 || len1 <= numRows) return s;
    int len2 = len1;
    char *s_zigzag = (char *)malloc(len1 + 1);
    int straight = 0, slash = 0, cnt = 0;
    if (numRows == 2){
        int mid = (len1 % 2)? (len1 / 2 + 1): (len1 / 2);
        for(; cnt < mid; cnt++){
            s_zigzag[cnt] = s[cnt * 2];
        }
        for (int i = 0; i < len1 - mid; i++, cnt++){
            s_zigzag[cnt] = s[i * 2 + 1];
        }
        s_zigzag[len1] = '\0';
        return s_zigzag;
    }
    while (len1 > 0){
        if(cnt % 2){
            len1 -= (numRows - 2);
            slash++;
        }
        else{
            len1 -= numRows;
            straight++;
        }
        cnt++;
    }
    char **sz = (char **)malloc(sizeof(char *) * (straight + slash));
    int sz_idx = 0;
    for (cnt = 0; cnt < len2; sz_idx++){
        sz[sz_idx] = (char *)malloc(numRows + 1);
        memset(sz[sz_idx], '!', numRows);
        if ((cnt % ((numRows - 1) << 1)) >= numRows ){ // slash string
            for (int i = (numRows - 2), j = 0; i > 0 ; i--, j++){
                if ((cnt + j) >= len2){
                    break;
                }
                sz[sz_idx][i] = s[cnt + j];
            }
            cnt += (numRows - 2);
        }
        else {
            for (int i = 0; i < numRows && (cnt + i) < len2; i++){
                sz[sz_idx][i] = s[cnt + i];
            }
            sz[sz_idx][numRows] = 0;
            cnt += numRows;
        }
    }
    int idx = 0;
    for (cnt = 0, sz_idx = 0; cnt < len2;){
        if(sz[sz_idx][idx] != '!' && sz[sz_idx][idx]){
            s_zigzag[cnt] = sz[sz_idx][idx];
            cnt++;
        }
        sz_idx++;
        if (sz_idx == (straight + slash)){
            sz_idx = 0;
            idx++;
        }
    }
    for (cnt = 0; cnt < (straight + slash); cnt++){
        free(sz[cnt]);
    }
    free(sz);
    s_zigzag[len2] = '\0';
    return s_zigzag;
}

int main(int argc, char **argv){
    char *s;
    s = convert(argv[1], *(argv[2]) - 48);
    printf("result = \n%s\n",s);
    free(s);
}
