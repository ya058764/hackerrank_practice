#include <stdio.h>
#include <string.h>

static char t[222] = {0};

int if_repeated(unsigned char s, int order){
    //static char t[222] = {0};
    int max = 0;
    if (!t[s - ' ']){
        t[s - ' '] = order;
        max = order;
    }
    else {
        for (int i = 0; i < 222; i++){
            if (t[i] == 0){
                continue;
            }
            else {
                if (i != (s - ' ')){    
                    t[i] -= t[s - ' '];
                    if (t[i] < 0){
                        t[i] = 0;
                    }
                    if (t[i] > max){
                        max = t[i];
                    }
                }
            }
        }
        t[s - ' '] = max + 1;
        max = t[s - ' '];
    }
    return max;
}

int lengthOfLongestSubstring(char * s){
    int  len = 0, str_len = strlen(s), max = 0;
    if (!str_len) return 0;
    for (int ptr = 0, cnt = 1; ptr < str_len; (max < len)? (ptr++, cnt++, max = len) : (ptr++, cnt = len + 1)){
        len = if_repeated((unsigned char)s[ptr], cnt);
        printf("\nlen = %d, scaned :%c\n", len, s[ptr]);
    }
    return max;
}

int main(void){
    char s[] = "pwwkew";
    printf("\nlen = %d\n",lengthOfLongestSubstring(s));
    strcpy(s, "bvbf");
    memset(t, 0, 222);
    printf("\nlen = %d\n",lengthOfLongestSubstring(s));
    strcpy(s, "    ");
    memset(t, 0, 222);
    printf("\nlen = %d\n",lengthOfLongestSubstring(s));
    strcpy(s, "");
    memset(t, 0, 222);
    printf("\nlen = %d\n",lengthOfLongestSubstring(s));
    strcpy(s, "abcabcbb");
    memset(t, 0, 222);
    printf("\nlen = %d\n",lengthOfLongestSubstring(s));
    return 0;
}
