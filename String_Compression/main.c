#include <string.h>
#include <stdio.h>

int compress(char* chars, int charsSize){
    char cmp = chars[0];
    int i = 1, cnt = 1, idx = 0, overten = 0;
    for (; i < charsSize && chars[i]; i++){
        if (cmp == chars[i]){
            cnt++;
        }
        else{
            chars[idx] = cmp;
            idx++;
            if (cnt > 999){
                chars[idx] = (cnt / 1000) + 48;
                idx++;
                cnt = cnt % 1000;
            }
            if (cnt > 99){
                chars[idx] = (cnt / 100) + 48;
                idx++;
                cnt = cnt % 100;
            }
            if (cnt > 9){
                overten = 1;
                chars[idx] = (cnt / 10) + 48;
                idx++;
                cnt = cnt % 10;
            }
            if (cnt > 1 || overten){
                chars[idx] = cnt + 48;
                idx++;
                overten = 0;
            }
            cnt = 1;
            cmp = chars[i];
        }
    }
    chars[idx] = cmp;
    idx++;
    if (cnt > 999){
        chars[idx] = (cnt / 1000) + 48;
        idx++;
        cnt = cnt % 1000;
    }
    if (cnt > 99){
        chars[idx] = (cnt / 100) + 48;
        idx++;
        cnt = cnt % 100;
    }
    if (cnt > 9){
        overten = 1;
        chars[idx] = (cnt / 10) + 48;
        idx++;
        cnt = cnt % 10;
    }
    if (cnt > 1 || overten){
        chars[idx] = cnt + 48;
        idx++;
        overten = 0;
    }
    return idx;
}


int main(int argc, char *argv[]){
    int len = strlen(argv[1]);
    len = compress(argv[1], len);
    printf("result = \nlength = %d\n",len);
    for (int i = 0; i < len; i++){
        printf("%c, ",argv[1][i]);
    }
}
