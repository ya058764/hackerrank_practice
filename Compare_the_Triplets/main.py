#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the compareTriplets function below.
def compareTriplets(a, b):
    x = 0
    y = 0
    iter = 0
    for ele_a in a:
        if ele_a > b[iter]:
            x += 1
        elif b[iter] > ele_a:
            y += 1
        iter = iter + 1
    return list([x,y])

if __name__ == '__main__':
    a = list([1,2,3])
    b = list([3,2,1])

    print(a)
    print(b)
    result = compareTriplets(a, b)
    print(result)
