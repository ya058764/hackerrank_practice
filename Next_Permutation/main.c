#include <stdio.h>

void swap(int *a,int *b){
	int tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

void selection_sort(int *arr,int size){
	int smallest,current,select_index,i,j;
	for (j = 0 ; j < size ; j++){
		smallest = *(arr + j);
		select_index = j;
	    for (i = j ; i < size ; i++){
	    	current = *(arr + i);
	        if ( current < smallest ){
	        	smallest = current;
	        	select_index = i;
			}
	    }
	    swap( (arr + j) , (arr + select_index) );
	}
}

void nextPermutation(int* nums, int numsSize){
    if (numsSize < 2){
        return;
    }
    int i = numsSize - 1, j = numsSize - 1;
    while(i > 0 && nums[i - 1] >= nums[i]){
        i--;
    }
    if (i < numsSize - 1){
        j = i;
    }
    else{
        swap(nums + (numsSize - 2), nums + (numsSize - 1));
        return;
    }
    printf("i = %d ,j = %d\n",i,j);
    selection_sort(nums + j, numsSize - j);
    if (j > 0){
        i = j;
        while (i < numsSize){
            if (nums[j - 1] < nums[i]){
                swap(nums + (j - 1), nums + i);
                printf("if\ni = %d ,j = %d\n",i,j);
                return;
            }
            i++;
        }
        printf("second\ni = %d ,j = %d\n",i,j);
        for (i = j - 1; i < numsSize - 1; i++){
            swap(nums + i, nums + (i + 1));
        }
    }
    return;
}

#define ARR_LEN 3
int main(void){
    //int arr[ARR_LEN] = {1,2,0,3,0,1,2,4};
    int arr[ARR_LEN] = {3,2,1};
    int i;
    for (i = 0; i < ARR_LEN; i++){
        printf("%2d",arr[i]);
    }
    printf("\n========================\n");
    nextPermutation(arr, ARR_LEN);
    for (i = 0; i < ARR_LEN; i++){
        printf("%2d",arr[i]);
    }
    while(1);
    return 0;
}
