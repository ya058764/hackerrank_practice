#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXNUM 9999999UL
#define CARRY  10000000UL

typedef struct part{
    unsigned int value;
    struct part *bigger_dir;
    struct part *smaller_dir;
}part_t;

void extra_large_multi(part_t **head, part_t **tail, int i);
void extraLongFactorials(int n);

void extra_large_multi(part_t **head, part_t **tail, int i){
    part_t *ptr = *tail;
    int overflow = 0;
    while (ptr){
        ptr->value = ptr->value * i + overflow;
        //printf("cal i = %d, val = %d\n head = %p, tail = %p\n",i,ptr->value,*head,*tail);
        if (ptr->value > MAXNUM){
            overflow = ptr->value / CARRY;
            ptr->value %= CARRY;
            if (!ptr->bigger_dir){
                ptr->bigger_dir = (part_t *)malloc(sizeof(struct part));
                ptr->bigger_dir->value = 0;
                ptr->bigger_dir->smaller_dir = ptr;
                ptr->bigger_dir->bigger_dir = NULL;
                *head = ptr->bigger_dir;
            }
        }
        else{
            overflow = 0;
        }
        ptr = ptr->bigger_dir;
    }
    return;
}

// Complete the extraLongFactorials function below.
void extraLongFactorials(int n) {
    //printf("start\n");
    part_t *head = NULL, *tail = NULL;
    part_t *ptr = (struct part *)malloc(sizeof(struct part));
    head = ptr;
    tail = ptr;
    ptr->value = 1;
    ptr->smaller_dir = NULL;
    ptr->bigger_dir = NULL;
    for (int i = 1; i <= n; i++){
        extra_large_multi(&head, &tail, i);
    }
    ptr = head;
    printf(" %p\n%d",ptr,ptr->value);
    while (ptr->smaller_dir){
        ptr = ptr->smaller_dir;
        if (ptr->value < 10){
            printf("000000");
        }
        else {
            if (ptr->value < 100){
                printf("00000");
            }
            else {
                if (ptr->value < 1000) {
                    printf("0000");
                }
                else {
                    if (ptr->value < 10000) {
                        printf("000");
                    }
                    else {
                        if (ptr->value < 100000) {
                            printf("00");
                        }
                        else {
                            if (ptr->value < 1000000) {
                                printf("0");
                            }
                        }
                    }
                }
            }
        }
        printf("%d",ptr->value);
    }
}

int main()
{
    int n = 45;
    printf("size of int = %d\n",sizeof(int));
    extraLongFactorials(n);
    while(1);
    return 0;
}
