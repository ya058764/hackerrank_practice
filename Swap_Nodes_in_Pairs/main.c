#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

struct ListNode{
    int val;
    struct ListNode *next;
};

struct ListNode* swapPairs(struct ListNode *head){
    if (!head){
        return head;
    }
    struct ListNode *lag = head;
    if (lag->next){
        struct ListNode *lead = lag->next;
        lag->next = lead->next;
        lead->next = lag;
        head = lead;
        while(lag->next && lag->next->next){
            lead = lag->next->next;
            lag->next->next = lead->next;
            lead->next = lag->next;
            lag->next = lead;
            lag = lead->next;
        }
    }
    return head;
}

struct ListNode* newNode(int val, struct ListNode *head){
    if (!head){
        struct ListNode *new = (struct ListNode *)malloc(sizeof(struct ListNode));
        if (new){
            new->val = val;
            new->next = NULL; 
            return new;
        }
        return NULL;
    }
    struct ListNode * ptr = head;
    for (; ptr->next; ptr = ptr->next);
    struct ListNode *new = (struct ListNode *)malloc(sizeof(struct ListNode));
    if (new){
        new->val = val;
        ptr->next = new;
        new->next = NULL;
    }
    return head;
}

void gothrough(struct ListNode *head){
    while(head){
        printf("%2d ",head->val);
        head = head->next;
    }
}

int main(void){
    struct ListNode *head = newNode(1, NULL);
    newNode(2, head);
    newNode(3,head);
    newNode(4,head);
    newNode(5,head);
    newNode(6,head);
    gothrough(head);
    printf("\n===========swap===========\n");
    gothrough(swapPairs(head));
    return 0;
}